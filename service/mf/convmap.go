package mf

import (
	bn "gitlab.com/alexia.shaowei/submodule/bignum"
	mrf "gitlab.com/alexia.shaowei/submodule/mapflect"
	"gitlab.com/alexia.shaowei/sw.webframe.shell/modules/responsedata"
	"gitlab.com/alexia.shaowei/sw.webframe.shell/registrar"
)

type Multiply struct {
	N1     string `mapflect:"x"`
	N2     string `mapflect:"y"`
	Answer string `mapflect:"val"`
}

func Compute(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {

	x, _ := bn.New("156156156.52651")
	y, _ := bn.New("1551561684789489561651489496")

	val := x.Multiply(y)

	var mpr map[string]string = map[string]string{
		"x":   x.String(),
		"y":   y.String(),
		"val": val.String(),
	}

	m1 := &Multiply{}
	err := mrf.Marshal(mpr, m1)
	if err != nil {
		return responsedata.CodeResponseFailureWithMessage(err.Error(), 5165156, "2jro23r08vj904t")
	}

	return responsedata.CodeResponseSuccess(m1)
}
