package jwt

import (
	"time"

	"gitlab.com/alexia.shaowei/sw.webframe.shell/modules/responsedata"
	jwt "gitlab.com/alexia.shaowei/sw.webframe.shell/modules/swjwt"
	"gitlab.com/alexia.shaowei/sw.webframe.shell/registrar"
)

type User struct {
	ID             int
	UID            int
	ChannelID      int
	Name           string
	NickName       string
	RoleID         int
	UserType       int
	UserStatus     int
	Timezone       int
	IsDelete       bool
	IsForbidden    bool
	LastLoginTime  int
	CreateUserName string
	CreateDate     int

	jwt.RegisteredClaimNames
}

func JWT(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {

	user := User{
		ID:        1,
		UID:       999,
		ChannelID: 5,
		Name:      "ChihHsi",
		NickName:  "None",

		RegisteredClaimNames: jwt.RegisteredClaimNames{
			ExpirationTime: time.Now().Add(time.Hour * 5).Unix(),
		},
	}

	token := jwt.New(jwt.SigningHS512, user)

	sign, err := token.Sign(servInfo.HSKey)
	if err != nil {
		return responsedata.CodeResponseFailureWithMessage(err.Error(), responsedata.ERROR_CODE_UNDEFINED, responsedata.ERROR_MSG_UNDEFINED)
	}

	return responsedata.CodeResponseSuccess(sign)
}

//http://127.0.0.1:8800/jwt/decrypt?token=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJJRCI6MSwiVUlEIjo5OTksIkNoYW5uZWxJRCI6NSwiTmFtZSI6IkNoaWhIc2kiLCJOaWNrTmFtZSI6Ik5vbmUiLCJSb2xlSUQiOjAsIlVzZXJUeXBlIjowLCJVc2VyU3RhdHVzIjowLCJUaW1lem9uZSI6MCwiSXNEZWxldGUiOmZhbHNlLCJJc0ZvcmJpZGRlbiI6ZmFsc2UsIkxhc3RMb2dpblRpbWUiOjAsIkNyZWF0ZVVzZXJOYW1lIjoiIiwiQ3JlYXRlRGF0ZSI6MCwiZXhwIjoxNjMzMzU1ODU2fQ.3POiYLAa3Jwf3izD-WtnTWf__ZuqGFFgZnMEgF0at6KsZLT3yfYiOTtPtfdRNq4hlTP1MMrZZFn6zT30gMpvEA
func JWTDecrypt(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {

	key := servInfo.HSKey
	token := paramReq.URIParam["token"][0]

	tk, err := jwt.Decrypt(token, func(token *jwt.JWToken) (interface{}, error) {
		return key, nil
	})

	if err != nil {
		return responsedata.CodeResponseFailure(999999, "Ojowefwefefjiojio")
	}

	return responsedata.CodeResponseSuccess(tk)
}
