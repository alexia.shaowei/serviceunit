package controller

import (
	router "gitlab.com/alexia.shaowei/sw.webframe.shell/registrar"
	"sw.weizhen/serviceunit/service/jwt"
	"sw.weizhen/serviceunit/service/mf"
)

func init() {
	router.Controller.AddRouter(router.GET, "/jwt", jwt.JWT)
	router.Controller.AddRouter(router.GET, "/jwt/decrypt", jwt.JWTDecrypt)

	router.Controller.AddRouter(router.GET, "/map", mf.Compute)

}
