package main

import (
	"gitlab.com/alexia.shaowei/sw.webframe.shell/server"
	_ "sw.weizhen/serviceunit/controller"
)

func main() {
	server.Deploy().Run()
}
